# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :admin_user do
    email { Faker::Internet.unique.email }
    password { Faker::Internet.password }
    super_admin true
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
  end
end
