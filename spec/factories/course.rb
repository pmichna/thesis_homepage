# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :course do
    name { Faker::Lorem.unique.word }
  end
end
