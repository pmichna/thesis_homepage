# frozen_string_literal: true

module SignInHelper
  def sign_in_admin(user)
    visit new_admin_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Sign in'
  end
end
