# frozen_string_literal: true

RSpec.describe 'Deleting course' do
  let(:admin) { create(:admin_user) }

  before do
    sign_in_admin(admin)
  end

  it 'deletes course' do
    create(:course)
    visit admin_courses_path
    click_link('Delete')
    expect(Course.count).to eq(0)
    expect(page).to have_content('Course destroyed successfully!')
  end
end
