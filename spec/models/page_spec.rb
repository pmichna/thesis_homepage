# frozen_string_literal: true

RSpec.describe Page do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:body) }
end
