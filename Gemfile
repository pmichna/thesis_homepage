# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.4.1'

# core
gem 'pg'
gem 'puma'
gem 'rails', '~> 5.1.4'

# assets
gem 'autoprefixer-rails'
gem 'bootstrap-sass'
gem 'coffee-rails'
gem 'jquery-rails'
gem 'js_cookie_rails'
gem 'momentjs-rails'
gem 'rails_bootstrap_sortable'
gem 'sass-rails'
gem 'select2-rails'
gem 'uglifier'

# others
gem 'best_in_place'
gem 'devise'
gem 'devise-i18n'
gem 'oj'
gem 'paperclip', '~> 6.0.0'
gem 'pundit'
gem 'rails-i18n'
gem 'ranked-model'
gem 'simple_form'
gem 'slim-rails'
gem 'tinymce-rails', '~> 4.7.5'
gem 'zipline'

group :production do
  gem 'unicorn'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'factory_bot_rails'
  gem 'rspec-rails'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'bullet'
  gem 'fasterer'
  gem 'listen'
  gem 'overcommit', require: false
  gem 'rubocop', require: false
end

group :test do
  gem 'capybara'
  gem 'faker', require: false
  gem 'shoulda-matchers'
end
