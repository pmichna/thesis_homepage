if ActiveRecord::Base.connection.data_source_exists? 'pages'
  Page.find_or_create_by!(homepage: true, language: :english) do |page|
    page.title = 'title'
    page.body = 'body'
    page.language = :english
  end

  Page.find_or_create_by!(homepage: true, language: :polish) do |page|
    page.title = 'tytuł'
    page.body = 'treść'
  end
end
