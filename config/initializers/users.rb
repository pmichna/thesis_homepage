if ActiveRecord::Base.connection.data_source_exists?('admin_users')
  AdminUser.find_or_create_by(super_admin: true) do |user|
    user.email = 'admin@example.com'
    user.password = 'password'
  end
end
