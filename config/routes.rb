# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :admin_users, path: 'admin_users', controllers: {
    passwords: 'admin_users/passwords',
    sessions: 'admin_users/sessions'
  }
  devise_for :users, path: 'users', controllers: {
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }

  root to: 'home#index'

  resources :password_attachments, only: [] do
    member do
      get :form
      post :download
    end
  end

  namespace :admin do
    root to: 'pages#index'
    resources :styles, only: [:index] do
      collection do
        put :update
        post :reset
      end
    end
    resources :settings, only: [:index] do
      collection do
        put :update
        post :reset
      end
    end
    resource :my_account, only: [:edit, :update]
    resources :pages, except: [:show]
    resources :attachments do
      collection do
        post :batch
      end
    end
    resources :courses, except: :new do
      resources :works
      resources :grades, only: [:index, :create]
      resources :messages, only: [:new, :create]
      member do
        get :grades_csv
      end
    end
    resources :users, only: [:index, :update, :destroy] do
      collection do
        delete :batch_delete
      end
    end
    resources :admin_users, except: [:show]
    resources :groups, only: [:index, :create, :update, :destroy]
    resources :courses_users, only: [:create, :destroy]
    post 'page_positions/:page_id/:direction', to: 'page_positions#create', as: 'page_positions'
    post 'course_positions/:course_id/:direction', to: 'course_positions#create', as: 'course_positions'
    resource :homepage, only: [:edit, :update]
    resources :attachment_courses, only: [:create, :destroy]
  end

  namespace :student do
    resources :attachments, only: [:index, :new, :create, :destroy, :update]
    resources :grades, only: [:index]
    resource :my_account, only: [:edit, :update]
  end

  resources :pages, only: :show do
    member do
      get :new_password
      post :set_password
    end
  end
end
