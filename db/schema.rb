# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180616094516) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_user_courses", force: :cascade do |t|
    t.bigint "admin_user_id", null: false
    t.bigint "course_id", null: false
    t.index ["admin_user_id"], name: "index_admin_user_courses_on_admin_user_id"
    t.index ["course_id"], name: "index_admin_user_courses_on_course_id"
  end

  create_table "admin_user_pages", force: :cascade do |t|
    t.bigint "admin_user_id", null: false
    t.bigint "page_id", null: false
    t.index ["admin_user_id"], name: "index_admin_user_pages_on_admin_user_id"
    t.index ["page_id"], name: "index_admin_user_pages_on_page_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "super_admin", default: false, null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "attachments", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string "userable_type"
    t.bigint "userable_id"
    t.bigint "course_id"
    t.string "password"
    t.index ["course_id"], name: "index_attachments_on_course_id"
    t.index ["userable_type", "userable_id"], name: "index_attachments_on_userable_type_and_userable_id"
  end

  create_table "course_users", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.index ["course_id"], name: "index_course_users_on_course_id"
    t.index ["user_id"], name: "index_course_users_on_user_id"
  end

  create_table "courses", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.integer "position"
    t.index ["name"], name: "index_courses_on_name", unique: true
  end

  create_table "grades", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "work_id"
    t.string "value"
    t.index ["user_id"], name: "index_grades_on_user_id"
    t.index ["work_id"], name: "index_grades_on_work_id"
  end

  create_table "groups", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", id: :serial, force: :cascade do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.boolean "homepage", default: false, null: false
    t.integer "language", null: false
    t.integer "other_language_page_id"
    t.integer "parent_id"
    t.integer "subpage_position"
    t.string "password"
    t.index ["parent_id"], name: "index_pages_on_parent_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "name", null: false
    t.string "value", null: false
    t.index ["name"], name: "index_settings_on_name", unique: true
  end

  create_table "styles", force: :cascade do |t|
    t.string "name", null: false
    t.string "value", null: false
    t.index ["name"], name: "index_styles_on_name", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", null: false
    t.string "encrypted_password", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "index_number"
    t.integer "group_id"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["group_id"], name: "index_users_on_group_id"
  end

  create_table "works", force: :cascade do |t|
    t.bigint "course_id", null: false
    t.string "name", null: false
    t.index ["course_id"], name: "index_works_on_course_id"
  end

  add_foreign_key "admin_user_courses", "admin_users"
  add_foreign_key "admin_user_courses", "courses"
  add_foreign_key "admin_user_pages", "admin_users"
  add_foreign_key "admin_user_pages", "pages"
  add_foreign_key "attachments", "courses"
  add_foreign_key "grades", "users"
  add_foreign_key "grades", "works"
  add_foreign_key "pages", "pages", column: "other_language_page_id"
  add_foreign_key "pages", "pages", column: "parent_id"
  add_foreign_key "users", "groups"
  add_foreign_key "works", "courses"
end
