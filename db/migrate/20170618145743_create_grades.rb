class CreateGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :grades do |t|
      t.references :user, foreign_key: true
      t.references :work, foreign_key: true
      t.string :value
    end
  end
end
