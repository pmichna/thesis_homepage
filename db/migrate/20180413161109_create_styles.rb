class CreateStyles < ActiveRecord::Migration[5.1]
  def change
    create_table :styles do |t|
      t.string :name, index: {unique: true}, null: false
      t.string :value, null: false
    end
  end
end
