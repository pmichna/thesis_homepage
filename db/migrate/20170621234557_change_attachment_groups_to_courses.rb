class ChangeAttachmentGroupsToCourses < ActiveRecord::Migration[5.1]
  def change
    drop_table :attachment_groups

    create_table :attachment_courses do |t|
      t.references :attachment, foreign_key: true, null: false
      t.references :course, foreign_key: true, null: false
    end
  end
end
