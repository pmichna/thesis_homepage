class RemoveNameFromAttachments < ActiveRecord::Migration[5.1]
  def change
    remove_column :attachments, :name
  end
end
