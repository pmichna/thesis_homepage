class AddNameToAdminUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :admin_users, :first_name, :string
    add_column :admin_users, :last_name, :string
    AdminUser.reset_column_information

    AdminUser.find_each do |au|
      au.update!(first_name: au.email, last_name: au.email)
    end

    change_column_null :admin_users, :first_name, false
    change_column_null :admin_users, :last_name, false
  end
end
