class CreateAdminUsersCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_user_courses do |t|
      t.references :admin_user, foreign_key: true, null: false
      t.references :course, foreign_key: true, null: false
    end
  end
end
