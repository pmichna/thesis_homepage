class DropAttachmentCourses < ActiveRecord::Migration[5.1]
  def change
    drop_table :attachment_courses
  end
end
