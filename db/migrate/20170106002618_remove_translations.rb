class RemoveTranslations < ActiveRecord::Migration[5.0]
  class Page < ActiveRecord::Base; end
  def change
    add_column :pages, :language, :integer, null: false
    add_column :pages, :other_language_page_id, :integer
    add_foreign_key :pages, :pages, column: :other_language_page_id
  end
end
