class RemoveHiddenFromAttachments < ActiveRecord::Migration[5.0]
  def change
    remove_column :attachments, :hidden
  end
end
