class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.string :name, null: false, index: {unique: true}
      t.string :value, null: false
    end
  end
end
