class CreateWorks < ActiveRecord::Migration[5.1]
  def change
    create_table :works do |t|
      t.references :group, foreign_key: true, null: false
      t.references :course, foreign_key: true, null: false
      t.string :name, null: false
    end
  end
end
