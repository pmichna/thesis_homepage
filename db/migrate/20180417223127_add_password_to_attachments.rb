class AddPasswordToAttachments < ActiveRecord::Migration[5.1]
  def change
    add_column :attachments, :password, :string
  end
end
