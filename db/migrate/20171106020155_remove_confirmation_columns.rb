class RemoveConfirmationColumns < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :confirmation_token
    remove_column :users, :confirmed_at
    remove_column :users, :confirmation_sent_at
    remove_column :users, :unconfirmed_email
    remove_column :users, :reset_password_token
    remove_column :users, :reset_password_sent_at

    remove_column :admin_users, :reset_password_token
    remove_column :admin_users, :reset_password_sent_at
  end
end
