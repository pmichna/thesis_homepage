class AddUserableToAttachments < ActiveRecord::Migration[5.1]
  def change
    add_reference :attachments, :userable, polymorphic: true
    remove_column :attachments, :user_id
  end
end
