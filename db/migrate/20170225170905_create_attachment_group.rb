class CreateAttachmentGroup < ActiveRecord::Migration[5.0]
  def change
    create_table :attachment_groups do |t|
      t.references :attachment, foreign_key: true
      t.references :group, foreign_key: true
    end
  end
end
