class AddHiddenToAttachments < ActiveRecord::Migration[5.0]
  def change
    add_column :attachments, :hidden, :boolean, default: true
  end
end
