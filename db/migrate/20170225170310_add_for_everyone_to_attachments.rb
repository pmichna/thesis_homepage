class AddForEveryoneToAttachments < ActiveRecord::Migration[5.0]
  def change
    add_column :attachments, :for_everyone, :boolean, null: false, default: false
  end
end
