class AddPasswordToPages < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :password, :string
  end
end
