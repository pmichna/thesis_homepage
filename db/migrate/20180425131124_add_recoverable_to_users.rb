class AddRecoverableToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :reset_password_token, :string, index: {unique: true}
    add_column :users, :reset_password_sent_at, :datetime
  end

  def down
    remove_columns :users, :reset_password_token, :reset_password_sent_at
  end
end
