# frozen_string_literal: true
class AddFilesToAttachments < ActiveRecord::Migration[5.0]
  def up
    add_attachment :attachments, :attachment
  end

  def down
    remove_attachment :attachments, :attachment
  end
end
