class CreateAdminUsersPages < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_user_pages do |t|
      t.references :admin_user, foreign_key: true, null: false
      t.references :page, foreign_key: true, null: false
    end
  end
end
