class RemoveAttachmentCourseNullConstraint < ActiveRecord::Migration[5.1]
  def change
    change_column_null :attachments, :course_id, true
  end
end
