class AddParentIdToPage < ActiveRecord::Migration[5.0]
  def change
    add_reference :pages, :parent, foreign_key: {to_table: :pages}
  end
end
