# frozen_string_literal: true
class AddDepartmentFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :first_name, :string, null: false
    add_column :users, :last_name, :string, null: false
    add_column :users, :index_number, :string, index: true
    add_column :users, :group, :string
  end
end
