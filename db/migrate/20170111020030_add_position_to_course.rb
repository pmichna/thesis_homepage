class AddPositionToCourse < ActiveRecord::Migration[5.0]
  class Course < ActiveRecord::Base; end
  def change
    add_column :courses, :position, :integer
    Course.all.each.with_index(1) do |course, index|
      course.update_column :position, index
    end
  end
end
