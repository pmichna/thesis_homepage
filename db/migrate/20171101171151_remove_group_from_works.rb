class RemoveGroupFromWorks < ActiveRecord::Migration[5.1]
  def change
    remove_column :works, :group_id
  end
end
