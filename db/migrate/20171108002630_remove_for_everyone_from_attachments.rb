class RemoveForEveryoneFromAttachments < ActiveRecord::Migration[5.1]
  def change
    remove_columns :attachments, :for_everyone
  end
end
