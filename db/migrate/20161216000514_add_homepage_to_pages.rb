class AddHomepageToPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :homepage, :boolean, default: false, null: false
  end
end
