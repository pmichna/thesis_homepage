class AddSubpagePositionToPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :subpage_position, :integer
  end
end
