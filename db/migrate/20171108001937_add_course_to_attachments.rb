class AddCourseToAttachments < ActiveRecord::Migration[5.1]
  def change
    add_reference :attachments, :course, foreign_key: true, null: false
  end
end
