# frozen_string_literal: true

class Style < ApplicationRecord
  ITEMS = {
    footer_background: '#A8C0D4',
    footer_color: '#fff',
    sidebar_item_background: '#f0f0f0',
    sidebar_item_color: '#000',
    sidebar_active_item_background: '#2C5463',
    sidebar_active_item_color: '#fff',
    sidebar_header_background: '#dcdcdc',
    sidebar_header_color: '#000',
    sidebar_border_color: '#2828281a',
    header_gradient_top: '#6c9fc0',
    header_gradient_bottom: '#31507f',
    header_upper_text_color: '#A8C0D4',
    header_lower_text_color: '#fff',
    body_background_color: '#fff',
    body_text_color: '#000',
    page_border_color: '#abc4d3'
  }.freeze

  validates :name, :value, presence: true

  def self.value_for(name)
    Style.find_or_create_by(name: name) do |style|
      style.value = ITEMS.fetch(name)
    end.value
  end
end
