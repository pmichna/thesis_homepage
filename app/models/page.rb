# frozen_string_literal: true

class Page < ApplicationRecord
  include RankedModel
  ranks :parent_order, column: :position, with_same: :language
  ranks :subpage_order, column: :subpage_position, with_same: :parent_id

  enum language: %i[english polish]

  validates :title, presence: true
  validates :body, presence: true
  validates :language, presence: true
  validate :only_one_homepage_per_language_can_exist
  validate :other_language_uniqueness
  validate :parent_language, if: :parent

  has_many :admin_user_pages, dependent: :destroy
  has_many :admin_users, through: :admin_user_pages, dependent: :destroy

  belongs_to :other_language_page, class_name: 'Page', optional: true # rubocop:disable Rails/InverseOf
  belongs_to :parent, class_name: 'Page', optional: true, inverse_of: 'subpages'
  has_many :subpages, class_name: 'Page', foreign_key: 'parent_id', dependent: :restrict_with_error,
                      inverse_of: 'parent'

  scope :without_homepage, -> { where(homepage: false) }
  scope :only_parents, -> { where(parent: nil) }
  scope :ordered_in_language, ->(lang) { where(language: lang).without_homepage.rank(:parent_order) }
  scope :polish, -> { where(language: 'polish') }
  scope :english, -> { where(language: 'english') }

  def self.homepage
    find_by(homepage: true)
  end

  def self.options_for_select
    order(:language, :position).map { |page| ["[#{page.language}] #{page.title}", page.id] }
  end

  def other_language
    english? ? :polish : :english
  end

  def parents_options_for_select
    pages = Page.order(:position).where.not(id: id).where(parent: nil).where(homepage: false)
    pages.map do |page|
      ["[#{page.language}] #{page.title}", page.id]
    end
  end

  private

  def parent_language
    return if parent.nil? || parent.language == language
    errors.add(:parent_id, 'Parent page has to be in the same language')
  end

  def other_language_uniqueness
    return if other_language_page.nil? || other_language_page.language != language
    errors.add(:other_language_page_id, 'Assigned page can not be in the same language')
  end

  def only_one_homepage_per_language_can_exist
    return unless validate_homepage_language?
    if multiple_polish_homepage?
      errors.add(:language, 'Polish homepage already exists')
    elsif multiple_english_homepage?
      errors.add(:language, 'English homepage already exists')
    end
  end

  def validate_homepage_language?
    homepage && language_changed?
  end

  def multiple_polish_homepage?
    language == 'polish' && !Page.polish.homepage.nil?
  end

  def multiple_english_homepage?
    language == 'english' && !Page.english.homepage.nil?
  end
end
