# frozen_string_literal: true

class AdminUserCourse < ApplicationRecord
  belongs_to :admin_user
  belongs_to :course
end
