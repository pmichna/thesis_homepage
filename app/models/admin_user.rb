# frozen_string_literal: true

class AdminUser < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable, :recoverable

  has_many :attachments, as: :userable, dependent: :destroy, inverse_of: :userable
  has_many :admin_user_pages, dependent: :destroy
  has_many :pages, through: :admin_user_pages, dependent: :destroy
  has_many :admin_user_courses, dependent: :destroy
  has_many :courses, through: :admin_user_courses, dependent: :destroy

  validates :first_name, :last_name, presence: true

  def full_name
    "#{first_name} #{last_name}"
  end

  def full_name_reversed
    "#{last_name} #{first_name}"
  end
end
