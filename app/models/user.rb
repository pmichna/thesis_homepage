# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :rememberable, :validatable, :confirmable,
         :recoverable

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :index_number, presence: true
  validates :group, presence: true
  validates :courses, presence: true

  has_many :course_users, dependent: :destroy
  has_many :courses, through: :course_users
  has_many :attachments, as: :userable, dependent: :destroy, inverse_of: :userable
  has_many :grades, dependent: :destroy

  belongs_to :group

  scope :order_by_last_name, -> { order('lower(last_name)') }

  def reverse_full_name
    "#{last_name} #{first_name}"
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def to_s
    reverse_full_name
  end
end
