# frozen_string_literal: true

class Grade < ApplicationRecord
  belongs_to :work
  belongs_to :user

  validates :user, uniqueness: { scope: :work }
  validates :value, presence: true

  scope :order_by_user_last_name, -> { joins(:user).order('lower(users.last_name)') }
  scope :order_by_work_name, -> { joins(:work).order('lower(works.name)') }
end
