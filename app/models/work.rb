# frozen_string_literal: true

class Work < ApplicationRecord
  belongs_to :course
  has_many :grades, dependent: :destroy

  validates :name, presence: true
end
