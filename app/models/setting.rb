# frozen_string_literal: true

class Setting < ApplicationRecord
  def self.items # rubocop:disable Metrics/MethodLength
    {
      english_page_title: 'Władysław Homenda',
      polish_page_title: 'Władysław Homenda',
      polish_titles_and_name: 'prof. nzw. dr hab. inż. Władysław Homenda',
      english_titles_and_name: 'PhD DSc Władysław Homenda, Associate Professor',
      left_logo_image_url: ActionController::Base.helpers.asset_url('logo_mini.png'),
      right_logo_image_url: ActionController::Base.helpers.asset_url('logo_pw.png'),
      left_logo_link_url: 'http://www.mini.pw.edu.pl/',
      right_logo_link_url: 'http://www.pw.edu.pl/',
      slides_enabled: 'true',
      page_border_enabled: 'true',
      page_background_enabled: 'false'
    }
  end

  def self.value_for(name)
    Setting.find_or_create_by(name: name) do |setting|
      setting.value = items.fetch(name)
    end.value
  end
end
