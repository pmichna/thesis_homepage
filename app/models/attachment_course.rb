# frozen_string_literal: true

class AttachmentCourse < ApplicationRecord
  belongs_to :attachment
  belongs_to :course
end
