# frozen_string_literal: true

class Group < ApplicationRecord
  has_many :users, dependent: :restrict_with_error
end
