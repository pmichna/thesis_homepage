# frozen_string_literal: true

class Attachment < ApplicationRecord
  belongs_to :userable, polymorphic: true
  belongs_to :course, optional: true

  has_attached_file :attachment,
                    url: '/system/:attachment/:id_partition/:username_:filename',
                    path: ':rails_root/public/system/:attachment/:id_partition/:username_:filename'

  validates :attachment, attachment_presence: true
  do_not_validate_attachment_file_type :attachment

  def addable_courses
    Course.where.not(id: course_ids).order('lower(name)')
  end

  def name_with_user
    "#{userable.last_name}#{userable.first_name.first}"
  end

  Paperclip.interpolates :username do |attachment, _style|
    attachment.instance.name_with_user
  end
end
