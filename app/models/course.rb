# frozen_string_literal: true

class Course < ApplicationRecord
  require 'csv'

  include RankedModel
  ranks :order, column: :position

  validates :name, presence: true, uniqueness: true

  has_many :course_users, dependent: :destroy
  has_many :users, through: :course_users
  has_many :admin_user_courses, dependent: :destroy
  has_many :admin_users, through: :admin_user_courses
  has_many :works, dependent: :destroy
  has_many :attachments, dependent: :nullify

  def grades_csv # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    attributes = %w[name index] + works.pluck(:name)

    CSV.generate(headers: true) do |csv|
      csv << attributes
      works = Work.where(course: self)
      users.each do |user|
        row = [user.reverse_full_name, user.index_number]
        grades = works.map { |work| work.grades.find_by(user: user)&.value || '-' }
        row.concat(grades)
        csv << row
      end
    end
  end
end
