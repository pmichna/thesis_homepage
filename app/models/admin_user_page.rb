# frozen_string_literal: true

class AdminUserPage < ApplicationRecord
  belongs_to :admin_user
  belongs_to :page
end
