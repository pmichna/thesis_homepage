# frozen_string_literal: true

module Accessible
  extend ActiveSupport::Concern

  included do
    before_action :check_user
  end

  protected

  def check_user
    if admin_user_signed_in?
      flash.clear
      redirect_to(admin_root_path) && return
    elsif user_signed_in?
      flash.clear
      redirect_to(root_path) && return
    end
  end
end
