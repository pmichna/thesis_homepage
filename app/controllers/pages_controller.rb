# frozen_string_literal: true

class PagesController < ApplicationController
  prepend_before_action :set_page

  def show
    redirect_to new_password_page_path(@page) unless password_ok?
  end

  def new_password
  end

  def set_password
    old_passwords = if cookies.signed[:page_passwords].blank?
                      {}
                    else
                      JSON.parse(cookies.signed[:page_passwords])
                    end
    old_passwords[@page.id] = params[:page][:password]
    cookies.signed[:page_passwords] = old_passwords.to_json
    if password_ok?
      redirect_to page_path(@page)
    else
      redirect_to new_password_page_path(@page), alert: t('flashes.wrong_password')
    end
  end

  private

  def password_ok?
    return true if @page.password.blank?
    return false if cookies.signed[:page_passwords].blank?
    @page.password == JSON.parse(cookies.signed[:page_passwords])[@page.id.to_s]
  end

  def set_page
    @page = Page.find(params[:id])
  end

  def set_other_language_page
    cookies[:other_language_page] = if @page.other_language_page
                                      page_path(@page.other_language_page)
                                    else
                                      Page.where(language: @page.other_language).homepage
                                    end
  end

  def set_locale
    I18n.locale = @page.english? ? :en : :pl
  end
end
