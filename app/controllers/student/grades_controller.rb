# frozen_string_literal: true

module Student
  class GradesController < BaseController
    def index
      @grades = current_user.grades.order_by_work_name
    end
  end
end
