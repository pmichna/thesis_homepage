# frozen_string_literal: true

module Student
  class AttachmentsController < BaseController
    def index
      @user_attachments = current_user.attachments.order(:created_at)
      @admin_users_attachments = Attachment.where(userable_type: AdminUser.name, course: current_user.courses) # rubocop:disable Metrics/LineLength
                                           .order(:created_at)
    end

    def new
      @attachment = Attachment.new
    end

    def create
      @attachment = Attachment.new(attachment_params)
      @attachment.userable = current_user
      if @attachment.save
        redirect_to student_attachments_path, notice: I18n.t('flashes.file_uploaded')
      else
        render :new
      end
    end

    def update
      @attachment = Attachment.find(params[:id])
      @attachment.update(attachment_params) if @attachment.userable == current_user
    end

    def destroy
      attachment = Attachment.find(params[:id])
      if attachment.userable == current_user && attachment.destroy
        redirect_to student_attachments_path, notice: I18n.t('flashes.file_uploaded')
      else
        redirect_to student_attachments_path, alert: attachment.errors.full_messages.join(', ')
      end
    end

    private

    def attachment_params
      params.require(:attachment).permit(:attachment, :course_id)
    end
  end
end
