# frozen_string_literal: true

module Student
  class BaseController < ApplicationController
    before_action :authenticate_user!
  end
end
