# frozen_string_literal: true

module Student
  class MyAccountsController < BaseController
    def edit; end

    def update # rubocop:disable Metrics/AbcSize
      if params[:user][:password].blank?
        params[:user].delete('password')
        params[:user].delete('password_confirmation')
      end
      if current_user.update(user_params)
        bypass_sign_in current_user, scope: :user
        redirect_to edit_student_my_account_path, notice: t('flashes.account_updated')
      else
        render :edit
      end
    end

    private

    def user_params
      params.require(:user)
            .permit(:first_name, :last_name, :email, :password, :password_confirmation, :index_number,
                    :group_id, course_ids: [])
    end
  end
end
