# frozen_string_literal: true

module Admin
  class SettingsController < AdminController
    def index
      authorize(Setting)
    end

    def update
      authorize(Setting)
      settings_params.each do |style_name, style_value|
        Setting.find_by(name: style_name).update(value: style_value)
      end
      redirect_to admin_settings_path, notice: 'Settings updated'
    end

    def reset
      authorize(Setting)
      Setting.items.each do |name, value|
        setting = Setting.find_or_create_by(name: name)
        setting.update(value: value)
      end
      redirect_to admin_settings_path, notice: 'Settings reset'
    end

    private

    def settings_params
      params.require(:settings).permit(Setting.items.keys)
    end
  end
end
