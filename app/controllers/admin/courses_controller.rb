# frozen_string_literal: true

module Admin
  class CoursesController < AdminController
    def index
      authorize(Course)
      @courses = Course.rank(:order)
      @new_course = Course.new
    end

    def create
      authorize(Course)
      course = Course.new(course_params)
      if course.save
        render_partial_to_json('admin/courses/_index_table', :ok, courses: Course.rank(:order).all)
      else
        render json: { errors: course.errors.messages[:name] }, status: :unprocessable_entity
      end
    end

    def edit
      authorize(Course)
      @course = Course.find(params[:id])
    end

    def update
      authorize(Course)
      @course = Course.find(params[:id])
      if @course.update(course_params)
        redirect_to admin_courses_path, notice: "Course #{@course.name} updated"
      else
        render :edit
      end
    end

    def destroy
      authorize(Course)
      @course = Course.find(params[:id])
      @course.destroy
      redirect_to admin_courses_path, notice: I18n.t('flashes.course_destroyed')
    end

    def grades_csv
      @course = Course.find(params[:id])
      respond_to do |format|
        format.html
        format.csv { send_data @course.grades_csv, filename: "grades-#{@course.name}-#{Time.zone.today}.csv" } # rubocop:disable Merics/LineLength
      end
    end

    private

    def course_params
      params.require(:course).permit(:name, admin_user_ids: [])
    end
  end
end
