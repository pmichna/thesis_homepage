# frozen_string_literal: true

module Admin
  class GradesController < AdminController
    def create
      params[:grades].each do |user_id, works_grades|
        works_grades.each do |work_id, grade_value|
          work = Work.find(work_id)
          user = User.find(user_id)
          set_grade(user, work, grade_value)
        end
      end
      course = Course.find(params[:course_id])
      redirect_to admin_course_grades_path(course), notice: "Grades for #{course.name} updated"
    end

    def index
      @course = Course.find(params[:course_id])
    end

    private

    def set_grade(user, work, grade_value)
      grade = Grade.find_by(user: user, work: work)
      return if grade.nil? && grade_value.blank?
      if grade.nil?
        new_grade = Grade.create(user: user, work: work, value: grade_value)
        UserMailer.with(grade: new_grade).new_grade_email.deliver_now
      elsif grade_value.blank?
        grade.destroy
      elsif grade.value != grade_value
        grade.update(value: grade_value)
        UserMailer.with(grade: grade).new_grade_email.deliver_now
      end
    end
  end
end
