# frozen_string_literal: true

module Admin
  class AdminController < ApplicationController
    include Pundit

    layout 'admin'
    before_action :authenticate_admin_user!
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private

    def set_locale
      I18n.locale = :en
    end

    def pundit_user
      current_admin_user
    end

    def user_not_authorized
      flash[:alert] = 'You are not authorized to perform this action.'
      redirect_to(request.referer || admin_root_path, only_path: true)
    end
  end
end
