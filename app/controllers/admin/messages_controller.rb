# frozen_string_literal: true

module Admin
  class MessagesController < AdminController
    def new
      @course = Course.find(params[:course_id])
    end

    def create # rubocop:disable Metrics/AbcSize
      subject = params[:message][:subject]
      body = params[:message][:body]
      course = Course.find(params[:course_id])
      course.users.each do |user|
        UserMailer.with(user: user, subject: subject, body: body).message_email.deliver_now
      end
      redirect_to(
        new_admin_course_message_path(course),
        notice: "Message sent successfully #{course.name} students"
      )
    end
  end
end
