# frozen_string_literal: true

module Admin
  class PagesController < AdminController
    before_action :set_page, only: %i[edit update destroy]
    def index
      @polish_pages = policy_scope(Page).ordered_in_language(:polish)
      @english_pages = policy_scope(Page).ordered_in_language(:english)
    end

    def new
      authorize Page
      page_to_copy = Page.find_by(id: params[:copy_page_id])
      @page = Page.new(language: params[:language] || page_to_copy&.language,
                       homepage: params[:homepage],
                       body: page_to_copy&.body,
                       title: page_to_copy&.title,
                       parent: page_to_copy&.parent)
      render :new
    end

    def create
      authorize Page
      @page = Page.new(page_params)
      if @page.save
        redirect_to admin_pages_path, notice: t('flashes.page_created')
      else
        render :new, locals: { creating_homepage: page_params[:homepage] }
      end
    end

    def edit
      authorize @page
    end

    def update
      authorize @page
      if @page.update(page_params)
        redirect_to admin_pages_path, notice: t('flashes.page_updated')
      else
        render :edit
      end
    end

    def destroy
      @page.other_language_page&.update!(other_language_page: nil)
      if @page.destroy
        redirect_to admin_pages_path, notice: t('flashes.page_destroyed')
      else
        redirect_to admin_pages_path, alert: @page.errors.full_messages.join(', ')
      end
    end

    private

    def set_page
      @page = Page.find(params[:id])
    end

    def page_params
      params.require(:page).permit(:title, :body, :homepage, :language, :other_language_page_id,
                                   :parent_id, :password)
    end
  end
end
