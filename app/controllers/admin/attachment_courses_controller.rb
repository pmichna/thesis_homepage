# frozen_string_literal: true

module Admin
  class AttachmentCoursesController < AdminController
    def create
      attachment_course = AttachmentCourse.new(attachment_course_params)
      if attachment_course.save
        render_partial_to_json(
          'admin/attachments/_attachment_courses',
          :ok,
          attachment_courses: attachment_course.attachment.attachment_courses
        )
      else
        render json: { errors: errors.attachment_course.errors.full_messages.join(', ') }
      end
    end

    def destroy
      attachment_course = AttachmentCourse.find(params[:id])
      attachment = attachment_course.attachment
      attachment_course.destroy
      render_partial_to_json(
        'admin/attachments/_courses_select_options',
        :ok,
        available_courses: attachment.addable_courses
      )
    end

    private

    def attachment_course_params
      params.require(:attachment_course).permit(:course_id, :attachment_id)
    end
  end
end
