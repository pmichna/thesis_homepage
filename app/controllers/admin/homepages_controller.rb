# frozen_string_literal: true

module Admin
  class HomepagesController < AdminController
    def edit
      @homepage = Page.where(language: language_param).find_by(homepage: true)
      if @homepage
        redirect_to edit_admin_page_path(@homepage)
      else
        redirect_to new_admin_page_path(homepage: true, language: language_param)
      end
    end

    private

    def language_param
      if %w[polish english].include?(params[:language])
        params[:language]
      else
        'english'
      end
    end
  end
end
