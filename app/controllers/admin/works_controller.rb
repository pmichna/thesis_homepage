# frozen_string_literal: true

module Admin
  class WorksController < AdminController
    def index
      @course = Course.find(params[:course_id])
      @works = @course.works.order(:name)
    end

    def create
      work = Work.new(work_params)
      work.course = Course.find(params[:course_id])
      if work.save
        render create_success_response(work)
      else
        render_partial_to_json('admin/works/_form', :unprocessable_entity, work: work) # rubocop:disable Metrics/LineLength
      end
    end

    def update
      work = Work.find(params[:id])
      work.update(work_params)
    end

    def destroy
      work = Work.find(params[:id])
      work.destroy
      redirect_to admin_course_works_path(work.course), notice: "#{work.name} destroyed"
    end

    def show
      @work = Work.find(params[:id])
      authorize(@work)
    end

    private

    def create_success_response(work)
      {
        json: {
          table_partial: render_to_string('admin/works/_index_table',
                                          layout: false,
                                          locals: { works: work.course.works.order(:name), course: work.course }), # rubocop:disable Metrics/LineLength
          form_partial: render_to_string('admin/works/_form',
                                         layout: false,
                                         locals: { work: Work.new, course: work.course }) # rubocop:disable Metrics/LineLength
        }, status: :ok
      }
    end

    def work_params
      params.require(:work).permit(:name)
    end
  end
end
