# frozen_string_literal: true

module Admin
  class UsersController < AdminController
    def index
      @users = if current_admin_user.super_admin?
                 users_array(User.order(:last_name).includes(:course_users).order(:last_name))
               else
                 users_array(
                   User.distinct.joins(:courses)
                                .where(courses: { id: current_admin_user.course_ids }).order(:last_name)
                 )
               end
    end

    def update
      user = User.find(params[:id])
      user.update(group_id: params[:group_id]) if params[:group_id]
    end

    def destroy
      authorize(User)
      user = User.find(params[:id])
      if user.destroy
        redirect_to admin_users_path, notice: "#{user.full_name} deleted"
      else
        redirect_to admin_users_path, alert: user.errors.full_messages.join(', ')
      end
    end

    def batch_delete # rubocop:disable Metrics/AbcSize
      return redirect_to admin_users_path, alert: 'No students selected' if params[:user].blank?
      users = User.where(id: params[:user][:ids])
      users.each do |user|
        raise Pundit::NotAuthorized unless UserPolicy.new(current_admin_user, user).destroy?
        user.destroy
      end
      redirect_to admin_users_path, notice: 'Selected students deleted'
    end

    private

    def users_array(users)
      users.map do |user|
        {
          user: user,
          course_users: user.course_users.joins(:course).order('courses.position'),
          available_courses: Course.where.not(id: user.course_ids).rank(:order)
        }
      end
    end
  end
end
