# frozen_string_literal: true

module Admin
  class GroupsController < AdminController
    def index
      authorize(Group)
      @groups = Group.all.order(:name)
      @new_group = Group.new
    end

    def create
      authorize(Group)
      group = Group.new(group_params)
      if group.save
        render_partial_to_json('admin/groups/_index_table', :ok, groups: Group.order(:name))
      else
        render json: { errors: group.errors.messages[:name] }, status: :unprocessable_entity
      end
    end

    def update
      authorize(Group)
      @group = Group.find(params[:id])
      @group.update(group_params)
    end

    def destroy
      group = Group.find(params[:id])
      if group.destroy
        redirect_to admin_groups_path, notice: "Group #{group.name} deleted"
      else
        redirect_to admin_groups_path, alert: group.errors.full_messages.join(', ')
      end
    end

    private

    def group_params
      params.require(:group).permit(:name)
    end
  end
end
