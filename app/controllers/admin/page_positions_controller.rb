# frozen_string_literal: true

module Admin
  class PagePositionsController < AdminController
    def create
      @page = Page.find(params[:page_id])
      if params[:direction] == 'up'
        move_up
      else
        move_down
      end
      render_partial_to_json('admin/pages/_index_table',
                             :ok,
                             pages: Page.ordered_in_language(@page.language))
    end

    private

    def move_up
      if @page.parent.nil?
        @page.update(parent_order_position: :up)
      else
        @page.update(subpage_order_position: :up)
      end
    end

    def move_down
      if @page.parent.nil?
        @page.update(parent_order_position: :down)
      else
        @page.update(subpage_order_position: :down)
      end
    end
  end
end
