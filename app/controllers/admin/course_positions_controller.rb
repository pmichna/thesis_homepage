# frozen_string_literal: true

module Admin
  class CoursePositionsController < AdminController
    def create
      @course = Course.find(params[:course_id])
      if params[:direction] == 'up'
        @course.update(order_position: :up)
      else
        @course.update(order_position: :down)
      end
      render_partial_to_json('admin/courses/_index_table', :ok, courses: Course.rank(:order))
    end
  end
end
