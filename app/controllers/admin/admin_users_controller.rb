# frozen_string_literal: true

module Admin
  class AdminUsersController < AdminController
    def index
      authorize(AdminUser)
      @admin_users = AdminUser.order('lower(last_name)')
    end

    def new
      authorize(AdminUser)
      @admin_user = AdminUser.new
    end

    def create
      authorize(AdminUser)
      @admin_user = AdminUser.new(admin_user_params)
      if @admin_user.save
        redirect_to admin_admin_users_path, notice: "Teacher #{@admin_user.full_name} created"
      else
        render :new
      end
    end

    def edit
      @admin_user = find_admin_user
      authorize @admin_user
    end

    def update # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      @admin_user = find_admin_user
      authorize(@admin_user)
      if params[:admin_user][:password].blank?
        params[:admin_user].delete('password')
        params[:admin_user].delete('password_confirmation')
      end
      if @admin_user.update(admin_user_params)
        redirect_to admin_admin_users_path, notice: "Teacher #{@admin_user.full_name} updated"
      else
        render :edit
      end
    end

    def destroy
      authorize(AdminUser)
      admin_user = find_admin_user
      if admin_user.destroy
        redirect_to admin_admin_users_path, notice: "Teacher #{admin_user.full_name} deleted"
      else
        redirect_to admin_admin_users_path, alert: admin_user.errors.full_messages.join(', ')
      end
    end

    private

    def find_admin_user
      AdminUser.find(params[:id])
    end

    def admin_user_params
      params.require(:admin_user)
            .permit(:password, :password_confirmation, :email, :first_name, :last_name,
                    page_ids: [], course_ids: [])
    end
  end
end
