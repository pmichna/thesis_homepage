# frozen_string_literal: true

module Admin
  class StylesController < AdminController
    def index
      authorize(Setting)
    end

    def update
      authorize(Setting)
      styles_params.each do |style_name, style_value|
        Style.find_by(name: style_name).update(value: style_value)
      end
      redirect_to admin_styles_path, notice: 'Colors updated'
    end

    def reset
      authorize(Setting)
      Style::ITEMS.each do |name, value|
        style = Style.find_or_create_by(name: name)
        style.update(value: value)
      end
      redirect_to admin_styles_path, notice: 'Colors reset'
    end

    private

    def styles_params
      params.require(:styles).permit(Style::ITEMS.keys)
    end
  end
end
