# frozen_string_literal: true

module Admin
  class AttachmentsController < AdminController
    include ActionController::Streaming
    include Zipline

    def create
      @attachment = Attachment.new(attachment_params.merge(userable: current_admin_user))
      if @attachment.save
        redirect_to admin_attachments_path, notice: 'Uploaded!'
      else
        render :new
      end
    end

    def index # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      @my_attachments = current_admin_user.attachments.order(:created_at)
      if current_admin_user.super_admin?
        @teachers_attachments = Attachment.where(userable_type: AdminUser.name)
                                          .where.not(userable_id: current_admin_user.id)
                                          .order(:created_at)
      end
      @students_attachments = if current_admin_user.super_admin?
                                Attachment.where(userable_type: User.name).order(:created_at)
                              else
                                Attachment.where(userable_type: User.name, course: current_admin_user.courses).order(:created_at) # rubocop:disable Metrics/LineLength
                              end
    end

    def new
      @attachment = Attachment.new
      @courses = courses
    end

    def edit
      @attachment = Attachment.find(params[:id])
      @courses = courses
    end

    def update
      @attachment = Attachment.find(params[:id])
      authorize(@attachment)
      if @attachment.update(attachment_params)
        redirect_to admin_attachments_path, notice: 'Attachment updated'
      else
        render :edit
      end
    end

    def destroy
      attachment = Attachment.find(params[:id])
      authorize(attachment)
      attachment.destroy
      redirect_to admin_attachments_path, notice: I18n.t('flashes.file_destroyed')
    end

    def batch # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      return redirect_to admin_attachments_path, alert: 'No attachments selected' if params[:attachments].blank? # rubocop:disable Metrics/LineLength
      attachments = Attachment.where(id: params[:attachments][:ids])
      if params[:delete]
        attachments.each do |attachment|
          raise Pundit::NotAuthorized unless AttachmentPolicy.new(current_admin_user, attachment).destroy? # rubocop:disable Metrics/LineLength
          attachment.destroy
        end
        redirect_to admin_attachments_path, notice: I18n.t('flashes.files_destroyed')
      elsif params[:download]
        files = attachments.map { |attachment| [attachment.attachment, attachment.attachment_file_name] } # rubocop:disable Metrics/LineLength
        zipline(files, 'files.zip')
      end
    end

    private

    def courses
      if current_admin_user.super_admin?
        Course.order('lower(name)')
      else
        current_admin_user.courses.order('lower(name)')
      end
    end

    def attachment_params
      params.require(:attachment).permit(:attachment, :course_id, :password)
    end
  end
end
