# frozen_string_literal: true

module Admin
  class MyAccountsController < AdminController
    def edit; end

    def update # rubocop:disable Metrics/AbcSize
      if params[:admin_user][:password].blank?
        params[:admin_user].delete('password')
        params[:admin_user].delete('password_confirmation')
      end
      if current_admin_user.update(admin_user_params)
        bypass_sign_in current_admin_user, scope: :admin_user
        redirect_to edit_admin_my_account_path, notice: 'Account updated'
      else
        render :edit
      end
    end

    private

    def admin_user_params
      params.require(:admin_user)
            .permit(:first_name, :last_name, :email, :password, :password_confirmation)
    end
  end
end
