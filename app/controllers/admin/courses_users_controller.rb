# frozen_string_literal: true

module Admin
  class CoursesUsersController < AdminController
    def create
      course = Course.find(params[:course_id])
      user = User.find(params[:user_id])
      course_user = CourseUser.new(user: user, course: course)
      if course_user.save
        render_partial_to_json('admin/users/_courses',
                               :ok,
                               course_users: user.course_users.joins(:course).order('courses.position'))
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    def destroy
      course_user = CourseUser.find(params[:id])
      course_user.user.grades.where(work: course_user.course.works.ids).destroy_all
      course_user.destroy
      render_partial_to_json(
        'admin/users/_courses_select_options',
        :ok,
        available_courses: Course.where.not(id: course_user.user.course_ids).rank(:order)
      )
    end
  end
end
