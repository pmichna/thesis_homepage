# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale
  before_action :set_other_language_page

  private

  def render_partial_to_json(partial, status, locals = {})
    render json: {
      partial: render_to_string(partial,
                                layout: false,
                                locals: locals)
    }, status: status
  end

  def set_other_language_page
    return unless request.get?
    other_language = I18n.locale == :pl ? 'en' : 'pl'
    cookies[:other_language_page] = request.path + "?locale=#{other_language}"
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(
      :sign_up, keys: [:first_name, :last_name, :index_number, :group_id, course_ids: []]
    )
  end

  def set_locale
    if language_change_necessary?
      I18n.locale = the_new_locale
      cookies[:locale] = locale.to_s
    else
      I18n.locale = cookies[:locale]
    end
  end

  def language_change_necessary?
    cookies[:locale].nil? || params[:locale]
  end

  def the_new_locale
    %w[en pl].include?(params[:locale]) ? params[:locale] : I18n.default_locale.to_s
  end

  def after_sign_in_path_for(resource)
    if admin_user_signed_in?
      admin_root_path
    else
      stored_location_for(resource) || student_grades_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope == :user
      new_user_session_path
    elsif resource_or_scope == :admin_user
      new_admin_user_session_path
    else
      root_path
    end
  end
end
