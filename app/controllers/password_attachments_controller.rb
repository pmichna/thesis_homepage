# frozen_string_literal: true

class PasswordAttachmentsController < ApplicationController
  def form
    @attachment = Attachment.find(params[:id])
  end

  def download
    attachment = Attachment.find(params[:id])
    password = params[:attachment][:password]
    if attachment.password == password
      send_file(attachment.attachment.path)
    else
      redirect_to form_password_attachment_path(attachment), alert: t('flashes.wrong_password')
    end
  end
end
