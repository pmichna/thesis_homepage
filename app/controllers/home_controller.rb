# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @page = Page.where(language: I18n.t('locale_name')).homepage
    render 'pages/show' unless @page.nil?
  end
end
