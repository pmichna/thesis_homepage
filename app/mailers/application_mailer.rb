# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'system@homenda.mini.pw.edu.pl'
  layout 'mailer'
end
