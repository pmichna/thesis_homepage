# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def message_email
    mail(to: params[:user].email, body: params[:body], subject: params[:subject])
  end

  def new_grade_email
    @grade = params[:grade]
    mail(to: @grade.user.email, subject: 'You have received a new grade / Otrzymałeś nową ocenę')
  end
end
