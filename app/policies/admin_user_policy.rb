# frozen_string_literal: true

class AdminUserPolicy < ApplicationPolicy
  def index?
    user.super_admin?
  end

  def create?
    user.super_admin?
  end

  def new?
    create?
  end

  def update?
    user.super_admin?
  end

  def edit?
    update?
  end

  def destroy?
    user.super_admin?
  end
end
