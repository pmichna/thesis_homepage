# frozen_string_literal: true

class GroupPolicy < ApplicationPolicy
  def index?
    user.super_admin?
  end

  def create?
    user.super_admin?
  end

  def update?
    user.super_admin?
  end
end
