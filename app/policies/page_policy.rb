# frozen_string_literal: true

class PagePolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.super_admin?
        scope.all
      else
        scope.joins(:admin_users).where(admin_users: { id: user.id })
      end
    end
  end

  def update?
    user.super_admin? || user.pages.include?(record)
  end

  def create?
    user.super_admin?
  end
end
