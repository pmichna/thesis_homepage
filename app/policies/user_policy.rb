# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def destroy?
    user.super_admin?
  end
end
