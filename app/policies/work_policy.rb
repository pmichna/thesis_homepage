# frozen_string_literal: true

class WorkPolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.super_admin?
        scope.all
      else
        scope.joins(course: :admin_users).where(admin_users: { id: user.id })
      end
    end
  end

  def show?
    user.super_admin? || user.courses.include?(record.course)
  end
end
