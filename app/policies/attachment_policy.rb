# frozen_string_literal: true

class AttachmentPolicy < ApplicationPolicy
  def update?
    user.super_admin? || record.userable == user
  end

  def destroy?
    user.super_admin? || record.userable == user
  end
end
