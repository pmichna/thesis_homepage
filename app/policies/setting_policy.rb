# frozen_string_literal: true

class SettingPolicy < ApplicationPolicy
  def index?
    user.super_admin?
  end

  def update?
    user.super_admin?
  end

  def reset?
    user.super_admin?
  end
end
