class Pages
  constructor: ->
    pages_list = $('.pages-list')
    pages_list.on 'ajax:success', "a[data-method='post']", (e, xhr) ->
      $(this).closest('.pages-list').html(xhr.partial)

$ ->
  new Pages if $('body.admin_pages.index').length > 0
