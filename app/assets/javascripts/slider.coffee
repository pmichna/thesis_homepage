class Slider
  constructor: ->
    $checkbox = $('#slider-checkbox')
    $slider = $('#slider')
    cookie = Cookies.get('slider')
    unless cookie == 'off'
      $slider.show()
    $checkbox.change ->
      if this.checked
        $slider.show()
        Cookies.remove('slider')
      else
        $slider.hide()
        Cookies.set('slider', 'off')

$  ->
  new Slider if $('#slider').length > 0
