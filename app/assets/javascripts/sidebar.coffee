class Sidebar
  constructor: ->
    $('#sidebar').on 'click', '.glyphicon-plus', ->
      $(@).removeClass('glyphicon-plus')
      $(@).addClass('glyphicon-minus')
      $(@).closest('.page').find('.subpages').removeClass('hidden').css('display', 'block')

    $('#sidebar').on 'click', '.glyphicon-minus', ->
      $(@).removeClass('glyphicon-minus')
      $(@).addClass('glyphicon-plus')
      $(@).closest('.page').find('.subpages').addClass('hidden')

$ ->
  new Sidebar if $('#sidebar').length > 0
