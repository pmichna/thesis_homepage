// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require best_in_place
//= require bootstrap-sprockets
//= require select2
//= require js.cookie
//= require spectrum
//= require moment
//= require bootstrap-sortable
//= require_tree .

$(document).ready(function() {
  $('.best_in_place').best_in_place();
  $('.select2').select2({theme: 'bootstrap'});

  var slideIndex = 0;
  if($('#slider').length > 0) {
    showSlides();
  }

  function showSlides() {
      var i;
      var slides = $('#slider img')
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      slideIndex++;
      if (slideIndex > slides.length) {slideIndex = 1}
      slides[slideIndex-1].style.display = 'block';
      setTimeout(showSlides, 4500);
  }
});
