class Users
  constructor: ->
    @setupCoursesSelect()
    @setupGroupSelect()
    @setupDeleting()
    $('.select-all').change ->
      if this.checked
        $(@).closest('table').find(':checkbox').prop('checked', true)
      else
        $(@).closest('table').find(':checkbox').prop('checked', false)

  setupGroupSelect: ->
    $('table').on 'change', 'select.group-select', (e) ->
      selected_group_id = @.value
      return if selected_group_id.length == 0
      url = $(@).data('url')
      $.ajax(
        url: url,
        method: 'PUT',
        data: { group_id: selected_group_id }
      )

  setupCoursesSelect: ->
    $('table').on 'change', 'select.courses-select', (e) ->
      selected_course_id = @.value
      return if selected_course_id.length == 0
      url = $(@).data('url')
      user_id = $(@).data('user-id')
      target = $(@).data('target')
      select_id = $(@).attr('id')
      $.ajax(
        url: url,
        method: 'POST',
        data: { course_id: selected_course_id, user_id: user_id }
      ).done (data) ->
        $("##{select_id} option[value='#{selected_course_id}']").remove()
        $("##{target}").html(data.partial)

  setupDeleting: ->
    $('table').on 'ajax:success', 'a', (e, xhr) ->
      courseItem = $(@).parent()
      select = $("##{$(@).closest('ol').data('target')}")
      courseItem.remove()
      select.html(xhr.partial)

$ ->
  new Users if $('body.index.admin_users').length > 0
