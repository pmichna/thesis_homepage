class CoursesIndex
  constructor: ->
    form = $('#new_course')
    form_group = form.find('.form-group')
    help_block = form.find('.help-block')

    $('body').on 'ajax:success', "a[data-method='post']", (e, xhr) ->
      $(this).closest('table').html(xhr.partial)

    form.on 'ajax:success', (event, xhr) ->
      table = $('#courses-table')
      if table.length > 0
        table.replaceWith(xhr.partial)
      else
        $('#no-courses').replaceWith(xhr.partial)
      form.find('input[type=text]').val('')
      form_group.removeClass('has-error')
      help_block.html('')

    form.on 'ajax:error', (event, xhr, status, error) ->
      form_group.addClass('has-error')
      help_block.html(xhr.responseJSON.errors[0])

$ ->
  new CoursesIndex if $('body.admin_courses.index').length > 0
