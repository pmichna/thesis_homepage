class Grades
  constructor: ->
    @$newGradeForm = $('#new_grade')
    @$gradesTable = $('#grades_table')
    @$noGrades = $('#no-grades')
    @$newGradeForm.on 'ajax:success', @onNewGradeFormSuccess
    @$newGradeForm.on 'ajax:error', @onNewGradeFormError

  onNewGradeFormSuccess: (_event, data, _status, _xhr) =>
    @$gradesTable.html(data.table_partial)
    @$newGradeForm.html(data.form_partial)
    @$noGrades.remove()

  onNewGradeFormError: (_event, xhr, _status, _error) =>
    @$newGradeForm.html(xhr.responseJSON.partial)

$ ->
  new Grades if $('body.admin_works.show').length > 0
