class Styles
  constructor: ->
    $('input[type=text]').spectrum
      preferredFormat: 'hex'
      showInput: true


$ ->
  new Styles if $('body.admin_styles').length > 0
