class Works
  constructor: ->
    @$newWorkForm = $('#new_work')
    @$worksTable = $('#works-table')
    @$noWorksAlert = $('#no-works')
    @$newWorkForm.on 'ajax:error', @onNewWorkError
    @$newWorkForm.on 'ajax:success', @onNewWorkSuccess

  onNewWorkError: (_event, xhr, _status, _error) =>
    @$newWorkForm.html(xhr.responseJSON.partial)

  onNewWorkSuccess: (_event, data, _status, _xhr) =>
    @$worksTable.html(data.table_partial)
    @$newWorkForm.html(data.form_partial)
    @$noWorksAlert.remove()

$ ->
  new Works if $('body.index.admin_works').length > 0
