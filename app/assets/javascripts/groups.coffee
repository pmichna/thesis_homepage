class Course
  constructor: ->
    form = $('#new_group')
    form_group = form.find('.form-group')
    help_block = form.find('.help-block')

    $('body').on 'ajax:success', "a[data-method='post']", (e, xhr) ->
      $(this).closest('table').html(xhr.partial)

    form.on 'ajax:success', (event, xhr) ->
      table = $('#groups-table')
      if table.length > 0
        table.replaceWith(xhr.partial)
      else
        $('#no-groups').replaceWith(xhr.partial)
      form.find('input[type=text]').val('')
      form_group.removeClass('has-error')
      help_block.html('')

    form.on 'ajax:error', (event, xhr, status, error) ->
      form_group.addClass('has-error')
      help_block.html(xhr.responseJSON.errors[0])

$ ->
  new Course if $('body.admin_groups.index').length > 0
