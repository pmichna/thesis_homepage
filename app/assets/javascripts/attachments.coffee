class Attachments
  constructor: ->
    $('.select-all').change ->
      if this.checked
        $(@).closest('table').find(':checkbox').prop('checked', true)
      else
        $(@).closest('table').find(':checkbox').prop('checked', false)

$ ->
  new Attachments if $('body.admin_attachments.index').length > 0
