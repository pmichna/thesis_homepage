# frozen_string_literal: true

module SettingsHelper
  def teacher_name
    if I18n.locale == :en
      Setting.value_for(:english_titles_and_name)
    else
      Setting.value_for(:polish_titles_and_name)
    end
  end

  def page_title
    if I18n.locale == :en
      Setting.value_for(:english_page_title)
    else
      Setting.value_for(:polish_page_title)
    end
  end
end
