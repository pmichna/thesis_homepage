# frozen_string_literal: true

module ApplicationHelper
  def body_class
    "#{params[:controller].tr('/', '_')} #{action_name}"
  end

  def current_sidebar_class(path)
    current_page?(path) ? 'current' : ''
  end

  def error_message(resource, attr)
    resource.errors.messages[attr].join(', ')
  end

  def lang_link
    image = I18n.locale == :pl ? image_tag('en.svg') : image_tag('pl.svg')
    link_to(image, cookies[:other_language_page], class: 'lang-link')
  end

  def pages_menu_items # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    items = [
      { label: t('layouts.admin.navbar.all_pages'), path: admin_pages_path }
    ]
    if policy(Page.polish.homepage).edit?
      items.concat([
                     {
                       label: t('layouts.admin.navbar.edit_polish_homepage'),
                       path: edit_admin_homepage_path(language: :polish)
                     },
                     {
                       label: t('layouts.admin.navbar.edit_english_homepage'),
                       path: edit_admin_homepage_path(language: :english)
                     }
                   ])
    end
    if policy(Page).create?
      items << { label: t('layouts.admin.navbar.new_page'), path: new_admin_page_path }
    end
    items
  end

  def teachers_menu_items
    [
      { label: t('layouts.admin.navbar.all_teachers'), path: admin_admin_users_path },
      { label: t('layouts.admin.navbar.new_teacher'), path: new_admin_admin_user_path }
    ]
  end

  def files_menu_items
    [
      { label: t('layouts.admin.navbar.all_files'), path: admin_attachments_path },
      { label: t('layouts.admin.navbar.new_file'), path: new_admin_attachment_path }
    ]
  end

  def settings_menu_items
    [
      { label: t('layouts.admin.navbar.colors'), path: admin_styles_path },
      { label: t('layouts.admin.navbar.other_settings'), path: admin_settings_path }
    ]
  end

  def admin_active_class(scope)
    params[:controller].split('/').last == scope
  end

  def other_lang(current_lang)
    current_lang.to_s == 'en' ? t('polish') : t('english')
  end

  def sidebar_link(label, path, options = {})
    content_tag 'div', class: 'page', style: "background-color: #{sidebar_item_background(path)};" do
      content_tag 'div', class: 'page-container' do
        concat(content_tag('div', '', class: 'margin'))
        options[:style] = "color: #{sidebar_item_color(path)};"
        concat(link_to(label, path, options))
      end
    end
  end

  def show_subpages?(parent_page)
    current_page?(page_path(parent_page)) || parent_page.subpages.any? { |subpage| current_page?(page_path(subpage)) } # rubocop:disable Metrics/LineLength
  end
end
