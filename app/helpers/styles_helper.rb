# frozen_string_literal: true

module StylesHelper
  def footer_style
    "background-color: #{Style.value_for(:footer_background)};
     color: #{Style.value_for(:footer_color)};"
  end

  def sidebar_item_color(item_path)
    if current_page?(item_path)
      Style.value_for(:sidebar_active_item_color)
    else
      Style.value_for(:sidebar_item_color)
    end
  end

  def sidebar_item_background(item_path)
    if current_page?(item_path)
      Style.value_for(:sidebar_active_item_background)
    else
      Style.value_for(:sidebar_item_background)
    end
  end

  def sidebar_header_style
    "background-color: #{Style.value_for(:sidebar_header_background)};
    color: #{Style.value_for(:sidebar_header_color)};"
  end

  def sidebar_style
    "border-color: #{Style.value_for(:sidebar_border_color)};
    background-color: #{Style.value_for(:sidebar_item_background)};"
  end

  def header_style
    "background: linear-gradient(#{Style.value_for(:header_gradient_top)}, #{Style.value_for(:header_gradient_bottom)});" # rubocop:disable Metrics/LineLength
  end

  def header_upper_text_style
    "color: #{Style.value_for(:header_upper_text_color)};"
  end

  def header_lower_text_style
    "color: #{Style.value_for(:header_lower_text_color)}"
  end

  def body_style
    style = "color: #{Style.value_for(:body_text_color)};
             background-color: #{Style.value_for(:body_background_color)};"
    if Setting.value_for(:page_background_enabled) == 'true'
      style += "background: #FFFFFF url(#{image_url('pw_background.jpg')}) no-repeat center top"
    end
    style
  end

  def container_style
    if Setting.value_for(:page_border_enabled) == 'false'
      'border: none;'
    else
      "border-color: #{Style.value_for(:page_border_color)};"
    end
  end
end
